package com.example.tubes.service.model;

public class HistoryPayload {
    public String order_id;
    public String course_id;
    public String source;
    public String destination;
    public String vehicle;
    public String order_datetime;
    public String course_datetime;
    public int[] seats;
    public int fee;
}

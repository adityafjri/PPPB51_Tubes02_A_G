package com.example.tubes.service.model;

public class CoursePayload {

    public String course_id;
    public String departure;
    public String destination;
    public String datetime;
    public String vehicle;
    public String num_seat;
    public int[] seats;
    public int fee;

    public CoursePayload(String course_id, String departure, String destination,
                         String datetime, String vehicle, String num_seat,
                         int[] seats, int fee){
        this.course_id = course_id;
        this.departure = departure;
        this.destination = destination;
        this.datetime = datetime;
        this.vehicle = vehicle;
        this.num_seat = num_seat;
        this.seats = seats;
        this.fee = fee;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getNum_seat() {
        return num_seat;
    }

    public void setNum_seat(String num_seat) {
        this.num_seat = num_seat;
    }

    public int[] getSeats() {
        return seats;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }
}

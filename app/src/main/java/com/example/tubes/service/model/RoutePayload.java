package com.example.tubes.service.model;

public class RoutePayload {
    public String source;
    public String destination;
    public int fee;
}

package com.example.tubes.service.model;

public class OrderRequest {
    String course_id;
    String seats;

    public OrderRequest(String course_id, String seats) {
        this.course_id = course_id;
        this.seats = seats;
    }
}

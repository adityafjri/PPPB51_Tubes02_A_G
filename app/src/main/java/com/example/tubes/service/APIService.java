package com.example.tubes.service;

import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.tubes.service.model.CoursePayload;
import com.example.tubes.service.model.HistoryPayload;
import com.example.tubes.service.model.LoginRequest;
import com.example.tubes.service.model.OrderRequest;
import com.example.tubes.service.model.RoutePayload;
import com.example.tubes.view.history.HistoryViewModel;
import com.example.tubes.view.login.LoginViewModel;
import com.example.tubes.view.pembayaran.PembayaranViewModel;
import com.example.tubes.view.pesan.PesanViewModel;
import com.example.tubes.view.seat.SeatViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class APIService {
    public static final String BASE_URL = "https://devel.loconode.com/pppb/v1/";
    private Gson gson;
    private Context context;

    public APIService(Context context) {
        gson = new Gson();
        this.context = context;
    }

    public void getRoutesTask(String token, PesanViewModel pesanViewModel) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                BASE_URL + "routes",
                null,
                response -> {
                    try {
                        JsonElement jsonPayload = new JsonParser().parse(response.getString("payload"));
                        Type listType = new TypeToken<List<RoutePayload>>() {}.getType();
                        List<RoutePayload> payloads = new Gson().fromJson(jsonPayload, listType);

                        pesanViewModel.succeedGetRoutes(payloads);
                    } catch (JSONException e) {
                        Toast.makeText(context, "Error when parsing route", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                },
                error -> pesanViewModel.failedGetRoutes()
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);

    }

    public void postLoginTask(LoginRequest loginRequest, LoginViewModel loginViewModel) {
        String json = gson.toJson(loginRequest);
        try {
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST,
                    "https://devel.loconode.com/pppb/v1/authenticate",
                    new JSONObject(json),
                    response -> {
                        try {
                            loginViewModel.succeedLogin(response.getString("token"));
                        } catch (JSONException e) {
                            Toast.makeText(context, "Error when getting token", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    },
                    error -> loginViewModel.failedLogin(error.getMessage())
            ) {
                @Override
                public String getBodyContentType() {
                    return "application/json";
                }
            };

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getHistoryTask(String token, HistoryViewModel historyViewModel){
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                BASE_URL + "orders?limit=10",
                null,
                response -> {
                    try {
                        JsonElement jsonPayload = new JsonParser().parse(response.getString("payload"));
                        Type listType = new TypeToken<List<HistoryPayload>>() {}.getType();
                        List<HistoryPayload> payloads = new Gson().fromJson(jsonPayload, listType);

                        historyViewModel.succeedGetHistory(payloads);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> historyViewModel.failedGetHistory()
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

    public void postOrderTask(String token, OrderRequest orderRequest, PembayaranViewModel pembayaranViewModel) {
        String json = gson.toJson(orderRequest);
        try {
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST,
                    BASE_URL + "orders",
                    new JSONObject(json),
                    response -> {
                        try {
                            pembayaranViewModel.succeedOrder(response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> pembayaranViewModel.failedOrder(error.getMessage())
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
            };
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getCoursesTask(String token, SeatViewModel seatViewModel, String source,
                               String destination, String vehicle, String date, String hour){
        Uri yaapapun = Uri.parse(BASE_URL + "courses?").buildUpon().appendQueryParameter("source", source).appendQueryParameter("destination", destination)
                .appendQueryParameter("vehicle", vehicle).appendQueryParameter("date", date).appendQueryParameter("hour", hour).build();
        String url = yaapapun.toString();
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                yaapapun.toString(),
                null,
                response -> {
                    try {
                        JSONArray jsonPayload = response.getJSONArray("payload");
                        JSONObject payload = jsonPayload.getJSONObject(0);
                        JSONArray jsonSeats = payload.getJSONArray("seats");
                        int[] seats = new int[jsonSeats.length()];
                        for (int i = 0; i < jsonSeats.length(); i++) {
                            seats[i] = jsonSeats.getInt(i);
                        }
//                        seat = new Seat(seats, payload.getString("course_id"));
                        CoursePayload coursePayload = new CoursePayload(
                                payload.getString("course_id"),
                                payload.getString("source"),
                                payload.getString("destination"),
                                payload.getString("datetime"),
                                payload.getString("vehicle"),
                                String.valueOf(payload.getInt("num_seats")),
                                seats,
                                payload.getInt("fee")
                        );
                        seatViewModel.succeedGetCourse(coursePayload);
                    }catch (JSONException e){
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                },
                error -> {
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}

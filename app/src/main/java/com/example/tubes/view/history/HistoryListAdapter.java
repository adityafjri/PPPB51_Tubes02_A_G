package com.example.tubes.view.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tubes.R;

import java.util.List;

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.ViewHolder> {
    private List<History> historyList;
    private Context context;

    public HistoryListAdapter(List<History> historyList, Context context) {
        this.historyList = historyList;
        this.context = context;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.history_item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDate, tvTime, tvRoute, tvVehicle, tvSeat, tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tv_history_date);
            tvTime = itemView.findViewById(R.id.tv_history_time);
            tvRoute = itemView.findViewById(R.id.tv_history_route);
            tvVehicle = itemView.findViewById(R.id.tv_history_vehicle);
            tvSeat = itemView.findViewById(R.id.tv_history_seats);
            tvPrice = itemView.findViewById(R.id.tv_history_price);
        }

        public void bind(int position) {
            String[] datetime = historyList.get(position).getDatetime().split(" ");
            String date = datetime[0];
            String time = datetime[1];
            String route = historyList.get(position).getDeparture() + " - " + historyList.get(position).getDestination();
            int[] seats = historyList.get(position).getSeat();
            StringBuilder formattedSeat = new StringBuilder();
            int price = historyList.get(position).getPrice();
            String formattedPrice = String.format("Rp. %,d", price);

            for (int i = 0; i < seats.length; i++) {
                formattedSeat.append(seats[i]);
                if (i+1 != seats.length) formattedSeat.append(", ");
            }

            tvDate.setText(date);
            tvTime.setText(time);
            tvRoute.setText(route);
            tvVehicle.setText(historyList.get(position).getVehicle());
            tvSeat.setText(formattedSeat);
            tvPrice.setText(formattedPrice);
        }
    }
}


package com.example.tubes.view.pesan;

import static com.example.tubes.view.login.LoginViewModel.TOKEN_KEY;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.tubes.service.APIService;
import com.example.tubes.service.model.RoutePayload;

import java.util.ArrayList;
import java.util.List;

public class PesanViewModel extends AndroidViewModel {
    private APIService apiService;
    private Context context;
    private SharedPreferences preferences;
    private MutableLiveData<List<RouteModel>> routes;
    private MutableLiveData<Boolean> isLoading;

    public PesanViewModel(@NonNull Application application) {
        super(application);

        this.preferences = application.getSharedPreferences("com.example.tubes.sharedprefs", Context.MODE_PRIVATE);
        this.context = application;
        this.apiService = new APIService(application);
        this.routes = new MutableLiveData<>();
        this.isLoading = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<List<RouteModel>> getRoutes() {
        return routes;
    }

    public void loadRoutes() {
        isLoading.setValue(true);
        String token = preferences.getString(TOKEN_KEY, "");
        apiService.getRoutesTask(token, this);
    }

    public void succeedGetRoutes(List<RoutePayload> payloads) {
        List<RouteModel> routeModelList = new ArrayList<>();
        for (RoutePayload payload : payloads) {
            routeModelList.add(new RouteModel(payload.source, payload.destination, payload.fee));
        }
        routes.setValue(routeModelList);
        isLoading.setValue(false);
    }

    public void failedGetRoutes() {
        Toast.makeText(context, "Gagal mendapatkan rute", Toast.LENGTH_SHORT).show();
    }
}

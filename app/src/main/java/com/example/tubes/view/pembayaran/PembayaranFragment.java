package com.example.tubes.view.pembayaran;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.example.tubes.R;
import com.example.tubes.databinding.FragmentPembayaranBinding;
import com.example.tubes.view.history.HistoryFragment;
import com.example.tubes.view.login.LoginViewModel;
import com.example.tubes.view.pesan.PesanFragment;

public class PembayaranFragment extends Fragment {

    FragmentPembayaranBinding binding;
    private PembayaranViewModel pembayaranViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.binding = FragmentPembayaranBinding.inflate(getLayoutInflater());
        this.pembayaranViewModel = new ViewModelProvider(this).get(PembayaranViewModel.class);
        String course_id = getArguments().getString("COURSE_ID", "");
        int[] seats = getArguments().getIntArray("SEATS");
        this.binding.btnPesan.setOnClickListener(v -> this.pembayaranViewModel.newOrder(course_id, seats));
        this.pembayaranViewModel.getOrderSuccess().observe(requireActivity(), orderSucceed -> {
            if (orderSucceed) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, new HistoryFragment())
                        .commit();
            }
        });

        return this.binding.getRoot();
    }
}

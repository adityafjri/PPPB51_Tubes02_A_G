package com.example.tubes.view.history;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.tubes.R;

import java.util.ArrayList;

public class HistoryFragment extends Fragment {
    private HistoryViewModel historyViewModel;
    private HistoryListAdapter historyListAdapter;
    private RecyclerView rvHistory;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        historyViewModel = new ViewModelProvider(this).get(HistoryViewModel.class);
        historyListAdapter = new HistoryListAdapter(new ArrayList<>(), getActivity());

        rvHistory = view.findViewById(R.id.rv_history);
        rvHistory.setHasFixedSize(true);
        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        rvHistory.setAdapter(historyListAdapter);

        historyViewModel.getHistoryList().observe(requireActivity(), histories -> historyListAdapter.setHistoryList(histories));

        progressBar = view.findViewById(R.id.pb_history);

        historyViewModel.getIsLoading().observe(requireActivity(), isLoading -> progressBar.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE));
    }

    @Override
    public void onResume() {
        super.onResume();
        historyViewModel.loadHistory();
    }
}
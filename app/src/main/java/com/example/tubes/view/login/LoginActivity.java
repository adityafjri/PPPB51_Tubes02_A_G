package com.example.tubes.view.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tubes.R;
import com.example.tubes.view.MainActivity;
import com.example.tubes.view.home.HomeFragment;
import com.example.tubes.view.login.LoginViewModel;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(v -> {
            String username = etUsername.getText().toString().trim();
            String password = etPassword.getText().toString().trim();
            if (!username.isEmpty() || !password.isEmpty()) {
                loginViewModel.login(username, password);
            } else {
                Toast.makeText(this, "Please fill form to login", Toast.LENGTH_SHORT).show();
            }
        });
        loginViewModel.getLoginToken().observe(this, token -> {
            if (!token.isEmpty()) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
        });
    }
}
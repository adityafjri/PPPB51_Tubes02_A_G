package com.example.tubes.view.login;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.volley.VolleyError;
import com.example.tubes.service.APIService;
import com.example.tubes.service.model.LoginRequest;

public class LoginViewModel extends AndroidViewModel {
    private APIService apiService;
    private Context context;
    public MutableLiveData<String> loginToken;
    private SharedPreferences preferences;
    public static final String TOKEN_KEY = "TOKEN_KEY";

    public LoginViewModel(@NonNull Application application) {
        super(application);
        preferences = application.getSharedPreferences("com.example.tubes.sharedprefs", Context.MODE_PRIVATE);

        this.context = application;
        this.loginToken = new MutableLiveData<>(preferences.getString(TOKEN_KEY, ""));
        apiService = new APIService(application);
    }

    public MutableLiveData<String> getLoginToken() {
        return loginToken;
    }

    public void succeedLogin(String token) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
        loginToken.setValue(token);
    }

    public void failedLogin(String error) {
        Toast.makeText(context, "Login Gagal", Toast.LENGTH_SHORT).show();
    }

    public void login(String username, String password) {
        apiService.postLoginTask(new LoginRequest(username, password), this);
    }
}


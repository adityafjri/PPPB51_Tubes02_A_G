package com.example.tubes.view.history;

import static com.example.tubes.view.login.LoginViewModel.TOKEN_KEY;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.tubes.service.APIService;
import com.example.tubes.service.model.HistoryPayload;

import java.util.ArrayList;
import java.util.List;

public class HistoryViewModel extends AndroidViewModel {
    private MutableLiveData<List<History>> historyList;
    private APIService apiService;
    private Context context;
    private MutableLiveData<Boolean> isLoading;

    public HistoryViewModel(@NonNull Application application) {
        super(application);

        this.historyList = new MutableLiveData<>();
        this.context = application;
        this.apiService = new APIService(application);
        this.isLoading = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<List<History>> getHistoryList() {
        return historyList;
    }

    public void loadHistory() {
        isLoading.setValue(true);
        SharedPreferences preferences = context.getSharedPreferences("com.example.tubes.sharedprefs", Context.MODE_PRIVATE);
        String token = preferences.getString(TOKEN_KEY, "");
        apiService.getHistoryTask(token, this);
    }

    public void succeedGetHistory(List<HistoryPayload> payloads) {
        List<History> list = new ArrayList<>();
        for (HistoryPayload payload : payloads) {
            list.add(new History(
                    payload.course_datetime,
                    payload.source,
                    payload.destination,
                    payload.vehicle,
                    payload.seats,
                    payload.fee
            ));
        }
        historyList.setValue(list);
        isLoading.setValue(false);
    }

    public void failedGetHistory() {
        Toast.makeText(context, "Gagal mengambil data", Toast.LENGTH_SHORT).show();
        isLoading.setValue(false);
    }
}

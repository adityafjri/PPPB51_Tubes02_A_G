package com.example.tubes.view.pesan;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.example.tubes.R;
import com.example.tubes.view.seat.SeatBesarFragment;
import com.example.tubes.view.seat.SeatKecilFragment;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class PesanFragment extends Fragment implements View.OnClickListener {
    private Button btnCari, btnDatePicker, btnTimePicker;
    private AutoCompleteTextView dropdownRoutes, dropdownVehicle;
    private PesanViewModel pesanViewModel;
    private ArrayAdapter<CharSequence> routeListAdapter, vehicleListAdapter;
    private MaterialDatePicker.Builder datePickerBuilder;
    private MaterialDatePicker datePicker;
    private MaterialTimePicker.Builder timePickerBuilder;
    private MaterialTimePicker timePicker;
    private TextView tvDate, tvTime;
    private ProgressBar pbPesan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pesan, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pesanViewModel = new ViewModelProvider(this).get(PesanViewModel.class);
        dropdownRoutes = view.findViewById(R.id.dropdown_route);
        dropdownVehicle = view.findViewById(R.id.dropdown_mobil);
        btnCari = view.findViewById(R.id.btn_pesan);
        btnDatePicker = view.findViewById(R.id.btn_date_picker);
        tvDate = view.findViewById(R.id.tanggal);
        btnTimePicker = view.findViewById(R.id.btn_time_picker);
        tvTime = view.findViewById(R.id.jam);
        pbPesan = view.findViewById(R.id.pb_pesan);

        pesanViewModel.loadRoutes();
        pesanViewModel.getRoutes().observe(requireActivity(), routes -> {
            String[] routeList = new String[routes.size()];
            for (int i = 0; i < routes.size(); i++) {
                routeList[i] = String.format("%s - %s      (Rp.%d)", routes.get(i).departure, routes.get(i).destination, routes.get(i).fee);
            }
            routeListAdapter = new ArrayAdapter<>(getActivity(), R.layout.dropdown_item_list, routeList);
            dropdownRoutes.setAdapter(routeListAdapter);
        });


        datePickerBuilder = MaterialDatePicker.Builder.datePicker();
        datePickerBuilder.setTitleText("SELECT A DATE");
        datePicker = datePickerBuilder.build();

        btnDatePicker.setOnClickListener(this);

        datePicker.addOnPositiveButtonClickListener(selection -> {
            TimeZone timeZone = TimeZone.getDefault();
            int offsetFromUTC = timeZone.getOffset(new Date().getTime() * -1);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            Date date = new Date((long) selection + offsetFromUTC);

            tvDate.setText(simpleDateFormat.format(date));
        });

        timePickerBuilder = new MaterialTimePicker.Builder();
        timePicker = timePickerBuilder.setTitleText("PILIH WAKTU").setHour(12).setTimeFormat(TimeFormat.CLOCK_24H).build();

        btnTimePicker.setOnClickListener(this);

        timePicker.addOnPositiveButtonClickListener(v -> tvTime.setText(String.format("%02d:%02d", timePicker.getHour(), timePicker.getMinute())));

        vehicleListAdapter = new ArrayAdapter<>(getActivity(), R.layout.dropdown_item_list, getResources().getStringArray(R.array.vehicle));
        dropdownVehicle.setAdapter(vehicleListAdapter);

        pesanViewModel.getIsLoading().observe(requireActivity(), isLoading -> pbPesan.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE));

        btnCari.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (v == btnDatePicker) {
            datePicker.show(getActivity().getSupportFragmentManager(), "DATE_PICKER");
        } else if (v == btnTimePicker) {
            Toast.makeText(getActivity(), "TEST", Toast.LENGTH_SHORT).show();
            timePicker.show(getActivity().getSupportFragmentManager(), "TIME_PICKER");
        } else if (v == btnCari) {
            if (dropdownRoutes.getText().toString().trim().isEmpty()
                    || tvDate.getText().toString().trim().isEmpty()
                    || tvTime.getText().toString().trim().isEmpty()
                    || dropdownVehicle.getText().toString().trim().isEmpty()
            ) Toast.makeText(getActivity(), "Mohon lengkapi form", Toast.LENGTH_SHORT).show();
            else {
                String route = dropdownRoutes.getText().toString().trim();
                String departure = route.split("\\s+")[0];
                String destination = route.split("\\s+")[2];
                String date = tvDate.getText().toString().trim();
                String time = tvTime.getText().toString().trim().split(":")[0];
                String vehicle = dropdownVehicle.getText().toString().trim();

//                Kirim value dari form ke halaman selanjutnya:
                Bundle pesan = new Bundle();
                pesan.putString("departure", departure);
                pesan.putString("destination", destination);
                pesan.putString("vehicle", vehicle);
                pesan.putString("date", date);
                pesan.putString("hour", time);

                if (vehicle.equals("Small")){
                    SeatKecilFragment seatKecilFragment = new SeatKecilFragment();
                    seatKecilFragment.setArguments(pesan);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, seatKecilFragment)
                            .addToBackStack(null)
                            .commit();
                }
                else{
                    SeatBesarFragment seatBesarFragment = new SeatBesarFragment();
                    seatBesarFragment.setArguments(pesan);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container, seatBesarFragment)
                            .addToBackStack(null)
                            .commit();
                }

//                canvaFragment.setArguments(pesan)
//                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.fragment_container, canvasFragment)
//                        .addToBackStack(null)
//                        .commit();
            }
        }
    }
}

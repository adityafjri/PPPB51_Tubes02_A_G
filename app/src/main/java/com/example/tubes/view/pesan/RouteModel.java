package com.example.tubes.view.pesan;

public class RouteModel {
    String departure;
    String destination;
    int fee;

    public RouteModel(String departure, String destination, int fee) {
        this.departure = departure;
        this.destination = destination;
        this.fee = fee;
    }

    public RouteModel(String departure, String destination) {
        this.departure = departure;
        this.destination = destination;
    }

    public RouteModel() {
    }
}

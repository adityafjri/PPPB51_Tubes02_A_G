package com.example.tubes.view.seat;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.example.tubes.R;
import com.example.tubes.databinding.FragmentSeatBinding;
import com.example.tubes.view.pembayaran.PembayaranFragment;

public class SeatKecilFragment extends Fragment implements View.OnTouchListener, View.OnClickListener{

    private SeatViewModel seatViewModel;
    private Bitmap bitmap;
    FragmentSeatBinding binding;
    private Canvas canvas;
    private Paint paintAvailable, paintNotAvai, paintChosenSeat, paintSeatNum;
    private Rect seat1, seat2, seat3, seat4, seat5, seat6;
    private GestureDetector detector;
    private int[] dipilih = {0, 0, 0, 0, 0, 0,}; // 0: tidak dipilih, 1: dipilih
    private int[] status = {0, 0, 0, 0, 0, 0}; // status kursi available/not avilable
    private int[] kursi = {6}; // kursi yang not available



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.seatViewModel = new ViewModelProvider(this).get(SeatViewModel.class);
        this.binding = FragmentSeatBinding.inflate(getLayoutInflater());
        this.binding.tvSize.setText("Small");

        String source = getArguments().getString("departure", "");
        String destination = getArguments().getString("destination", "");
        String vehicle = getArguments().getString("vehicle", "Large");
        String date = getArguments().getString("date", "");
        String hour = getArguments().getString("hour", "00");

        seatViewModel.getCourse(source, destination, vehicle, date, hour);
        seatViewModel.getCoursePayload().observe(requireActivity(), coursePayload -> {
            this.kursi = coursePayload.seats;
            setCanvas(status, kursi);
        });

        this.detector = new GestureDetector(this.getContext(), new SeatKecilFragment.GestureListener());
        this.binding.ivMain.setOnTouchListener(this);
        this.binding.btnLanjut.setOnClickListener(this);

        // Inflate the layout for this fragment
        return this.binding.getRoot();
    }

    public void setCanvas(int[] status, int[] kursi){
        // 1. Create Bitmap
        bitmap = Bitmap.createBitmap(400, 540, Bitmap.Config.ARGB_8888);

        // 2. Associate the bitmap to the ImageView
        this.binding.ivMain.setImageBitmap(bitmap);

        // 3. Create a Canvas with the bitmap
        this.canvas = new Canvas(bitmap);

        // 4. Draw Canvas
        // Fill the entire canvas with this solid color
        int mColorBackground = ResourcesCompat.getColor(getResources(),
                R.color.white, null);
        canvas.drawColor(mColorBackground);

        // create style seat available
        paintAvailable = new Paint();
        int colorSeatAvai = ResourcesCompat.getColor(getResources(),
                R.color.pale_green, null);
        paintAvailable.setColor(colorSeatAvai);

        // create style seat not available
        paintNotAvai = new Paint();
        int colorSeatNotAvai = ResourcesCompat.getColor(getResources(),
                R.color.red, null);
        paintNotAvai.setColor(colorSeatNotAvai);

        //create style chosen seat
        paintChosenSeat = new Paint();
        int colorChosenSeat = ResourcesCompat.getColor(getResources(),
                R.color.yellow, null);
        paintChosenSeat.setColor(colorChosenSeat);

        // create style seat number
        paintSeatNum = new Paint();
        int colorSeatNum = ResourcesCompat.getColor(getResources(),
                R.color.black, null);
        paintSeatNum.setTextSize(26);
        paintSeatNum.setColor(colorSeatNum);

        for (int i = 0; i < kursi.length; i++){
            if (kursi[i] == 1){
                status[kursi[i] - 1] = 1;
            }
            else if (kursi[i] == 2){
                status[kursi[i] - 1] = 1;
            }
            else if (kursi[i] == 3){
                status[kursi[i] - 1] = 1;
            }
            else if (kursi[i] == 4){
                status[kursi[i] - 1] = 1;
            }
            else if (kursi[i] == 5){
                status[kursi[i] - 1] = 1;
            }
            else{
                status[kursi[i] - 1] = 1;
            }
        }

        for (int i = 0; i < 6; i++){
            if(i == 0){ // seat 1
                seat1 = new Rect(40, 20, 120, 100);
                if (status[i] == 0){
                    canvas.drawRect(seat1, paintAvailable);
                }
                else{
                    canvas.drawRect(seat1, paintNotAvai);
                }
                canvas.drawText("1", 72, 68, paintSeatNum); // 48, 32
            }
            else if (i == 1){ // seat 2
                seat2 = new Rect(40, 120, 120, 200);
                if (status[i] == 0){
                    canvas.drawRect(seat2, paintAvailable);
                }
                else{
                    canvas.drawRect(seat2, paintNotAvai);
                }
                canvas.drawText("2", 72, 168, paintSeatNum);
            }
            else if (i == 2){ // seat 3
                seat3 = new Rect(280, 120, 360, 200);
                if (status[i] == 0){
                    canvas.drawRect(seat3, paintAvailable);
                }
                else{
                    canvas.drawRect(seat3, paintNotAvai);
                }
                canvas.drawText("3", 312, 168, paintSeatNum);
            }
            else if (i == 3){ // seat 4
                seat4 = new Rect(40, 220, 120, 300);
                if (status[i] == 0){
                    canvas.drawRect(seat4, paintAvailable);
                }
                else{
                    canvas.drawRect(seat4, paintNotAvai);
                }
                canvas.drawText("4", 72, 268, paintSeatNum);
            }
            else if (i == 4){ // seat 5
                seat5 = new Rect(160, 220, 240, 300);
                if (status[i] == 0){
                    canvas.drawRect(seat5, paintAvailable);
                }
                else{
                    canvas.drawRect(seat5, paintNotAvai);
                }
                canvas.drawText("5", 192, 268, paintSeatNum);
            }
            else if (i == 5) { // seat 6
                seat6 = new Rect(280, 220, 360, 300);
                if (status[i] == 0) {
                    canvas.drawRect(seat6, paintAvailable);
                } else {
                    canvas.drawRect(seat6, paintNotAvai);
                }
                canvas.drawText("6", 312, 268, paintSeatNum);
            }
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        this.detector.onTouchEvent(event);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == this.binding.btnLanjut){
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, new PembayaranFragment())
                    .addToBackStack(null)
                    .commit();
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent e) {
            float x = e.getX()/3;
            float y = e.getY()/3;
//            Toast.makeText(getContext(), "x = " + x + " and y = " + y, Toast.LENGTH_SHORT).show();

            if (y > 20 && y < 100){ // baris 1
                if (x > 40 && x < 120){ // seat 1
                    if (dipilih[0] == 0 && status[0] == 0){
                        canvas.drawRect(seat1, paintChosenSeat);
                        canvas.drawText("1", 72, 68, paintSeatNum);
                        dipilih[0] = 1;
                    }
                    else if (dipilih[0] == 1 && status[0] == 0){
                        canvas.drawRect(seat1, paintAvailable);
                        canvas.drawText("1", 72, 68, paintSeatNum);
                        dipilih[0] = 0;
                    }
                }
            }
            else if(y > 120 && y < 200){ // baris 2
                if (x > 40 && x < 120){ // seat 2
                    if (dipilih[1] == 0 && status[1] == 0){
                        canvas.drawRect(seat2, paintChosenSeat);
                        canvas.drawText("2", 72, 168, paintSeatNum);
                        dipilih[1] = 1;
                    }
                    else if (dipilih[1] == 1 && status[1] == 0){
                        canvas.drawRect(seat2, paintAvailable);
                        canvas.drawText("2", 72, 168, paintSeatNum);
                        dipilih[1] = 1;
                    }
                }
                else if (x > 250 && x < 310){ // seat 3
                    if (dipilih[2] == 0 && status[2] == 0){
                        canvas.drawRect(seat3, paintChosenSeat);
                        canvas.drawText("3", 312, 168, paintSeatNum);
                        dipilih[2] = 1;
                    }
                    else if (dipilih[2] == 1 && status[2] == 0){
                        canvas.drawRect(seat3, paintAvailable);
                        canvas.drawText("3", 312, 168, paintSeatNum);
                        dipilih[2] = 0;
                    }
                }
            }
            else if (y > 220 && y < 300){ // baris 3
                if (x > 40 && x < 120){ // seat 4
                    if (dipilih[3] == 0 && status[3] == 0){
                        canvas.drawRect(seat4, paintChosenSeat);
                        canvas.drawText("4", 72, 268, paintSeatNum);
                        dipilih[3] = 1;
                    }
                    else if (dipilih[3] == 1 && status[3] == 0){
                        canvas.drawRect(seat4, paintAvailable);
                        canvas.drawText("4", 72, 268, paintSeatNum);
                        dipilih[3] = 0;
                    }
                }
                else if (x > 160 && x < 240){ // seat 5
                    if (dipilih[4] == 0 && status[4] == 0){
                        canvas.drawRect(seat5, paintChosenSeat);
                        canvas.drawText("5", 192, 268, paintSeatNum);
                        dipilih[4] = 1;
                    }
                    else if (dipilih[4] == 1 && status[4] == 0){
                        canvas.drawRect(seat5, paintAvailable);
                        canvas.drawText("5", 192, 268, paintSeatNum);
                        dipilih[4] = 0;
                    }
                }
                else if (x > 250 && x < 310){ // seat 6
                    if (dipilih[5] == 0 && status[5] == 0){
                        canvas.drawRect(seat6, paintChosenSeat);
                        canvas.drawText("6", 312, 268, paintSeatNum);
                        dipilih[5] = 1;
                    }
                    else if (dipilih[5] == 1 && status[5] == 0){
                        canvas.drawRect(seat6, paintAvailable);
                        canvas.drawText("6", 312, 268, paintSeatNum);
                        dipilih[5] = 0;
                    }
                }
            }

            binding.ivMain.invalidate();
            return super.onDown(e);
        }
    }
}

package com.example.tubes.view.history;

public class History {
    private String datetime, departure, destination, vehicle;
    private int[] seat;
    private int price;

    public History(String datetime, String departure, String destination, String vehicle, int[] seat, int price) {
        this.datetime = datetime;
        this.departure = departure;
        this.destination = destination;
        this.vehicle = vehicle;
        this.seat = seat;
        this.price = price;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public int[] getSeat() {
        return seat;
    }

    public void setSeat(int[] seat) {
        this.seat = seat;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

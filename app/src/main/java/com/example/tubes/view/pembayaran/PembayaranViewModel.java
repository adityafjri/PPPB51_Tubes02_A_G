package com.example.tubes.view.pembayaran;

import static com.example.tubes.view.login.LoginViewModel.TOKEN_KEY;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.tubes.service.APIService;
import com.example.tubes.service.model.OrderRequest;

public class PembayaranViewModel extends AndroidViewModel {

    private APIService apiService;
    private Context context;
    private SharedPreferences preferences;
    private MutableLiveData<Boolean> orderSuccess;
    public PembayaranViewModel(@NonNull Application application) {
        super(application);
        this.preferences = application.getSharedPreferences("com.example.tubes.sharedprefs", Context.MODE_PRIVATE);

        this.context = application;
        this.apiService = new APIService(application);
        this.orderSuccess = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> getOrderSuccess() {
        return orderSuccess;
    }

    public void newOrder (String course_id, int[] seat) {
        String token = preferences.getString(TOKEN_KEY, "");
        StringBuilder formattedSeat = new StringBuilder();
        for (int i = 0; i < seat.length; i++) {
            formattedSeat.append(seat[i]);
            formattedSeat.append(i+1 == seat.length ? "" : ", ");
        }
        apiService.postOrderTask(token, new OrderRequest(course_id, formattedSeat.toString()), this);
    }

    public void succeedOrder(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        orderSuccess.setValue(true);
    }

    public void failedOrder(String error) {
        Toast.makeText(context, "Gagal pesan", Toast.LENGTH_SHORT).show();
        orderSuccess.setValue(false);
    }
}

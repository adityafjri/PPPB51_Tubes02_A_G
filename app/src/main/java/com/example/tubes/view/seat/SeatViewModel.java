package com.example.tubes.view.seat;

import static com.example.tubes.view.login.LoginViewModel.TOKEN_KEY;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.tubes.service.APIService;
import com.example.tubes.service.model.CoursePayload;
import com.example.tubes.view.pesan.RouteModel;

import java.util.List;

public class SeatViewModel extends AndroidViewModel {

    private APIService apiService;
    private Context context;
    private SharedPreferences preferences;
    private MutableLiveData<CoursePayload> coursePayload;
    public SeatViewModel(@NonNull Application application) {
        super(application);

        this.preferences = application.getSharedPreferences("com.example.tubes.sharedprefs", Context.MODE_PRIVATE);
        this.context = application;
        this.apiService = new APIService(application);
        this.coursePayload = new MutableLiveData<>();
    }

    public MutableLiveData<CoursePayload> getCoursePayload() {
        return coursePayload;
    }

    public void getCourse(String source, String destination, String vehicle, String date, String hour) {
        String token = preferences.getString(TOKEN_KEY, "");
        apiService.getCoursesTask(token, this, source, destination, vehicle, date, hour);
    }

    public void succeedGetCourse(CoursePayload coursePayload) {
        this.coursePayload.setValue(coursePayload);
    }
}
